terraform {
  backend "s3" {
    bucket         = "burn-elect-api-devops-tfstate-0911"
    key            = "burn-elect.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "burn-elect-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.50.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}
