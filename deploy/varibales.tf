variable "prefix" {
  default = "bead"
}

variable "project" {
  default = "burn-elect-api-devops"
}

variable "contact" {
  default = "geeks@technicality.co.nz"
}

variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "burn-elect-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "395421589573.dkr.ecr.us-east-1.amazonaws.com/burn-elect-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for API"
  default     = "395421589573.dkr.ecr.us-east-1.amazonaws.com/burn-elect-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "stripe_secret_key" {
  description = "Secret key for Stripe app"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "burnelectric.co.nz"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}
