asgiref>=3.2.7,<3.2.8
Django>=3.0.8,<3.0.9 
djangorestframework>=3.11.0,<3.12.0 
flake8>=3.6.0,<3.7.0
Pillow>=5.3.0,<5.4.0
uwsgi>=2.0.18,<2.1.0
pytz>=2020.1,<2020.2
stripe>=2.48.0,<3.0
boto3>=1.12.0,<1.13.0 
django-storages>=1.9.1,<1.10.0 

psycopg2>=2.7.5,<2.8.0